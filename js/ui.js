/* Handles All UI Management */

class UI{

	constructor(){
		this.init();
	}
	
	// Initiates several functions on page load
	init(){
		document.addEventListener('DOMContentLoaded' , () => {
			this.listArticles();

			this.pagination();
		});
	}

	// List Articles
	listArticles(articlesPerPage = 10, currentPage = 1){
		let errorData = {};
		const articles = articlesAPI.getArticles(articlesPerPage, currentPage);

		articles
		.then(response => {

			let articlesHTML = '';

			response.forEach((value, index) => {

				let {
					avatar = 'https://s3.amazonaws.com/uifaces/faces/twitter/sprayaga/128.jpg',
					author = 'Anonymous',
					createAt = '-- classified --',
					id, title, url
				} = value;

				articlesHTML += `
				<div class="article">
					<h2 class="article-title">
						<a href="article.html?id=${id}">${title}</a>
					</h2>
					<div class="article-info">
						<div class="author">
							Author: <a href="${url}" target="_blank"><img src="${avatar}" width="28" height="28"> <span>${author}</span></a>
						</div>
						<div class="date">Created On: ${createAt}</div>
					</div>
				</div>`;
			});

			document.getElementById('article-list').innerHTML = articlesHTML;

		})
		.catch(error => {
			errorData = {
				'title': 'Connection Error',
				'message': 'Please check your connection. Automatically retrying in 5 seconds...'
			};

			this.errorHandler(errorData);

			setTimeout(() => {
				this.listArticles();
			}, 5000);
		});

	}

	errorHandler(errorData){
		
		if(typeof document.querySelector('.error-message') !== 'undefined' && document.querySelector('.error-message') !== null ){
			document.querySelector('.error-message').remove();
		}

		let {
			title = 'Error!',
			message = 'Something Went Wrong.'
		} = errorData;

		const errorMessage = `
			<div class="error-message">
				<h3>${title}</h3>
				<p>
					${message} <br>
					If the problem persists, please <a href="#" id="refresh-page">refresh<a> the page.
				</p>
			</div>`;

		document.getElementById('alert-message').innerHTML = errorMessage;
		document.getElementById('refresh-page').addEventListener('click', (e) => {
			e.preventDefault();
			location.reload();
		});

		setTimeout(() => {
			document.querySelector('.error-message').remove();
		}, 5000);
	}

	// Pagination - Next/Prev (This will be called in the List Articles function)
	pagination(currentPage = 1){
		let btns = document.querySelectorAll('.control');
		let page = 1;

		btns.forEach((button, index) => {
			button.addEventListener('click', (e) => {
				e.preventDefault();

				if(e.target.dataset.gotoPage > 0){
					page = parseInt(e.target.dataset.gotoPage);
					let newPrev, newNext = 0;

					this.listArticles(10, page);

					if(e.target.id === 'btn-prev'){
						newPrev = page - 1;
						newNext = newPrev + 2;
					}else if(e.target.id === 'btn-next'){
						newNext = page + 1;
						newPrev = newNext - 2;
					}

					document.getElementById('btn-prev').setAttribute('data-goto-page', newPrev);
					document.getElementById('btn-next').setAttribute('data-goto-page', newNext);
				}

			});
		});
	}
}