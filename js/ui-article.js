class UIArticleView{

	constructor(){
		this.init();
	}
	
	// Initiates several functions on page load
	init(){
		document.addEventListener('DOMContentLoaded' , () => {
			this.showArticle();
			this.showArticleComments();
			// this.showArticleImages();
			
		});
	}

	// Get Article
	showArticle(){
		let errorData = {};

		const url = new URL(window.location.href);
		const articleId = url.searchParams.get('id');

		const article = articlesAPI.getArticle(articleId);

		article
		.then(response => {
			let {
				avatar = 'https://s3.amazonaws.com/uifaces/faces/twitter/sprayaga/128.jpg',
				author = 'Anonymous',
				createAt = '-- classified --',
				id, title, url
			} = response;

			document.getElementById('article-title').textContent = title;
			document.getElementById('article-author').innerHTML = `
				<div class="author">
					Author: <a href="${url}" target="_blank"><img src="${avatar}" width="28" height="28"> <span>${author}</span></a> | <a href="#" id="edit-article">Edit Article</a>
				</div>`;
			document.querySelector('.loader').remove();
			
			this.editArticle();
		})
		.catch(error => {

			console.log(error);
			errorData = {
				'title': 'Cannot Load Article',
				'message': 'Something went wrong.'
			};

			this.errorHandler(errorData);
			document.querySelector('.loader').remove();
		});
	}

	editArticle(){
		let errorData = {};

		const url = new URL(window.location.href);
		const articleId = url.searchParams.get('id');
		
		if(typeof document.getElementById('edit-article') != 'undefined' && typeof document.getElementById('edit-article') != null){
			document.getElementById('edit-article').addEventListener('click', (e) => {
				e.preventDefault();

				e.target.classList.add('show');
			});
		}
	}

	// List Articles
	showArticleComments(){
		let errorData = {};

		const url = new URL(window.location.href);
		const articleId = url.searchParams.get('id');

		const comments = articlesAPI.getArticleComments(articleId);

		comments
		.then(response => {

			let commentsHTML = '';

			response.forEach((value, index) => {

				let {
					avatar = 'https://s3.amazonaws.com/uifaces/faces/twitter/sprayaga/128.jpg',
					name = 'Anonymous',
					createAt = '-- classified --',
					comment = '',
					id
				} = value;

				commentsHTML += `

				<div class="comment">
					<div class="left-column">
						<img src="${avatar}" alt="${name}" width="64" height="64">
					</div>
					<div class="right-column">
						<div class="comment-author">${name}</div>
						<div class="comment-date">${createAt}</div>
						<div class="comment-text">${comment}</div>
						<div class="comment-controls">
							<a href="#" class="edit-comment" data-comment-id="${id}">Edit</a> | 
							<a href="#" class="edit-delete" data-comment-id="${id}">Delete</a>
						</div>
					</div>
				</div>`;
			});

			document.getElementById('comments').innerHTML = commentsHTML;

		})
		.catch(error => {

			console.log(error);
			errorData = {
				'title': 'Cannot Load Comments',
				'message': 'Something went wrong.'
			};

			this.errorHandler(errorData)
		});

	}

	// Slider
	
	// Error Handling
	errorHandler(errorData){
		
		if(typeof document.querySelector('.error-message') !== 'undefined' && document.querySelector('.error-message') !== null ){
			document.querySelector('.error-message').remove();
		}

		let {
			title = 'Error!',
			message = 'Something Went Wrong.'
		} = errorData;

		const errorMessage = `
			<div class="error-message">
				<h3>${title}</h3>
				<p>
					${message} <br>
					If the problem persists, please <a href="#" id="refresh-page">refresh<a> the page.
				</p>
			</div>`;

		document.getElementById('alert-message').innerHTML = errorMessage;
		document.getElementById('refresh-page').addEventListener('click', (e) => {
			e.preventDefault();
			location.reload();
		});

		setTimeout(() => {
			document.querySelector('.error-message').remove();
		}, 5000);
	}

}