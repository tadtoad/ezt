/* Handles All API Calls */

class ArticlesAPI{

	// Pull Articles
	async getArticles(limit = 10, page = 1){

		let url = '';


		if(limit === 0){
			// url = 'local_json_temp.json'; // Test Only
			url = 'http://5e0df4b536b80000143db9ca.mockapi.io/etranzact/v1/article';
		}else{
			// url = 'local_json_temp_paginated.json'; // Test Only
			url = 'http://5e0df4b536b80000143db9ca.mockapi.io/etranzact/v1/article?page=' + page + '&limit=' + limit;
		}

		const articles = await fetch(url);
		const response = await articles.json();

		return response;
	}

	// Pull Article
	async getArticle(id = 1){

		let url = 'http://5e0df4b536b80000143db9ca.mockapi.io/etranzact/v1/article/' + id;

		const article = await fetch(url);
		const response = await article.json();

		return response;
	}

	// Pull Images
	async getArticleImages(id = 1){

		let url = 'http://5e0df4b536b80000143db9ca.mockapi.io/etranzact/v1/article/' + id + '/images';

		const images = await fetch(url);
		const response = await images.json();

		return response;
	}

	// Pull Comments
	async getArticleComments(id = 1){

		let url = 'http://5e0df4b536b80000143db9ca.mockapi.io/etranzact/v1/article/' + id + '/comments';

		const comments = await fetch(url);
		const response = await comments.json();

		return response;
	}

	// Add Article (Validate content first)
	/**
	 * Pass POST through fetch options
	 */

	// Edit Article (Validate content first)
	/**
	 * Pass POST through fetch options
	 */

	// Add Comments (Validate content first)
	/**
	 * Pass POST through fetch options
	 */

	// Edit Comments (Validate content first)
	/**
	 * Pass POST through fetch options
	 */

	// Delete Comments
	/**
	 * Pass DELETE through fetch options
	 */

}